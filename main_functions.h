//main_functions Header File


/************************************************************
 * Initialization
 *
 ************************************************************
 */
	/*These functions under the Initialization section are 
	 * used to interpret the data given in the input file.
	 * If you are having a problem with input interpretation,
	 * then here is the place to take it up.
	 *
	 */
void 	initialize_master_process_list(
		process_control_block**	process_list, 
		unsigned int*		process_count,
		int 			argNum, 
		char** 			mainArgs		);

char* 	loadInputFileToString(int argc, char** arvg);

unsigned int  	initialize_process_list( 
			process_control_block** process_list, 
			char* 			input_string);
	
	int   	__get_first_four(
			char* 		process_string, 
			unsigned int*	process_number, 
			unsigned char*	current_cpu, 
			unsigned char*	required_cpu,  
			unsigned int*	deadline		);
	


/*********************************************************
 * Simulation
 *
 *********************************************************
 */

void	get_increment(
		core_control_block*	core,
		process_control_block*	process_list,
		unsigned int		process_count,
		unsigned int		quantum,
		unsigned int*		increment,
		unsigned int		clock_tick	);
	/*Finds the next time to increment the system clock by.
	 * I found it a little easier to implement this in a way that
	 * does not garantee that each increment will be the most it can be.
	 * This is because the disk that is being operated on is not easily 
	 * found, both in the rr and edf scenario, so we scan them all.
	 * So, sometimes the increment will be smaller than it could be.
	 * How ever, this is no harm to the operation of the program.
	 *
	 */

void 	update_time(
		core_control_block*	core,
		process_control_block*	process_list,
		unsigned int		process_count,
		table_block*		table,
		unsigned int		increment,
		unsigned int*		clock_tick	);
	/*This increment's reach resource queue/list's proceses
	 * remaining_instruction_time.
	 * For the dvd, printer and table resource, statuses are updated
	 * as well.
	 */

void 	update_device_queues( 
		core_control_block*	core, 
		table_block* 		table		);
	/*This procedure ends up only updating the table queue.
	 *
	 */

void	reschedule_migrators_rr(
		core_control_block* 	core,
		process_control_block*	process_list,
		unsigned int		process_count	);

void	reschedule_migrators_edf(
		core_control_block 	core,
		process_control_block	process_list,
		unsigned int		process_count	);
	/*Both of the above functions deal with migrators, scheduling
	 * them appropriate to their respective scheduling schema.
	 * A migrating process is on that is either moving between cores,
	 * or is one that is finished with a resources and awaits to be scheduled
	 * onto it's resource queue.
	 * Note that this is not the same as READY state, where the process is on the
	 * queue, but awaits to be run.
	 *
	 */

void	update_core_instructions(
		core_control_block*	core,
		unsigned int		process_count  	);
	/*Processes on the core's times are incremented
	 */

void	execute_core_instructions(
		core_control_block*	core,
		printer_block*		printers,
		table_block*		table,
		process_queue_node**	disk_queue,
		unsigned char		sched_scheme	);
	/*Instructions are carried out. This involves sending processes to the 
	 * resources they are requesting.
	 */

void	context_switch_RR(
		core_control_block*	core,
		unsigned int		clock_tick,
		unsigned int		increment	);

void	context_switch_edf(
		core_control_block*	core,
		unsigned int		clock_tick,
		unsigned int		increment	);
	/*When a process has become blocked for IO, Completed, or preempted,
	 * a context switch must occur. The above procedures carry this out 
	 * according to the appropriate scheduling algorithm being used.
	 */


/******************************************************
 * Output
 ******************************************************
 */


void 	print_process_list( 
		const char*		filename,
		process_control_block* 	process_list, 
		unsigned int 		number_of_processes 	);
