//Core Control Block Header File


/*
 * This file is meant to contain the definition of the core control block struct and all
 * functions that will operate on it.
 */


/******************************************************************
 * Data Structures
 *
 ******************************************************************
 */

	// Core Control Block Structure
	typedef struct core_control_block 	core_control_block;
	
	// Process Queue Node Structure
	typedef struct process_queue_node 	process_queue_node;
		/*This struct provides a queue to the core_control_block
		 * to be used to keep track of processes on the core.
		 * It will be used both in the RR and the EDF scheduling schemas.
		 * Under RR, the queue will simply constantly be rotating while,
		 * under EDF, the queue will serve to keep track of the deadlines 
		 * of the processes in it.
		 * Under EDF, the head of the queue shall be the process with the 
		 * earliest deadline.
		 *
		 */

/****************************************************************
 * Core Operations
 *
 ****************************************************************
 */

void	schedule_process_rr(	core_control_block* 	core, 
				process_control_block* 	process_list ); 
	/*This procedure adds a process to the end of the queue.
	 * First in for the queue will always be the process with the higher
	 * process_id.
	 *
	 */

void 	schedule_process_edf(	core_control_block*	core, 
				process_control_block*	process );
	/*This procedure will schedule a process on the queue in its 
	 * appropriate prosition, which is by order of their deadlines.
	 * I think it is interesting to note that recursion can be used here
	 * to reschedule a prempted process.
	 *
	 */

/****************************************************************
 * Process Queue Operations
 *
 ****************************************************************
 */

void	enqueue_process( process_queue_node** queue_head, process_control_block* proc);
	/*This procedure merely tacks on a process to the end of a given queue.
	 */
