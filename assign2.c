//OSC 4330 Operating Systems - Programming Assignment 2
//Fall 2012 - Due Date: Monday, November 12, 2012 //Dylan Thompson - 1000260

//Process Scheduling Simulator

//System Headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

//Constants
#define QUANTUM		10
#define NETWORK_DELAY 	15
#define ERROR(a, b) 	{puts (a); exit (b);}

//Local Headers
#include "process_control_block.h"
#include "process_control_block.c"
#include "core_control_block.h"
#include "core_control_block.c"
#include "io_devices.c"
#include "main_functions.h"
#include "main_functions.c"






int main(int argc, char** arvg){

//******************************************
//  Data Structure Declaration and Initialization
	
	// Process List
	/*process_control_block* 	master_process_list;*/
	process_control_block*	process_list;
	unsigned int 		process_count;
	initialize_master_process_list( &process_list, &process_count, argc, arvg);	

	// Resources
	core_control_block 	core[4] = { {NULL, NULL},{NULL, NULL},
					    {NULL, NULL},{NULL, NULL} 	};
	printer_block* 		printers = malloc(sizeof(printer_block*));
	table_block		table[4] = { {0, NULL},{0, NULL},{0, NULL},{0, NULL} };
	process_queue_node*	disk_queue;

	// Clock
	unsigned int		clock_tick;
	int i; //Looping variable
	int complete = 1;

        remove( "CPU-EDF<=>DISK-EDF.txt" );
        remove( "CPU-EDF<=>DISK-RR.txt"  );
        remove( "CPU-RR<=>DISK-EDF.txt"  );
        remove( "CPU-RR<=>DISK-RR.txt"   );


//***************************************
//Simulation
	//*******************************
	//Cpu  RR
	//Disk RR
	
	//Initialization
	clock_tick = 0;
	
	//IO Device initialization
	printers->printer1 = NULL;
	printers->printer2 = NULL;
	printers->print_queue_head = NULL;
	disk_queue = NULL;

	//Scheduling
	for( i = 0; i < process_count; i++ )
		schedule_process_rr( core, process_list + i );	

	print_process_list( "CPU-RR<=>DISK-RR.txt", process_list, process_count );
	
	/*complete = 1;*/
	//Start clock
	while( complete != 0 ){

		int increment = 0;
		get_increment( 	core, 		process_list, 	process_count,
				QUANTUM,	&increment, 	clock_tick	);

		update_time( 	core, 		process_list, 	process_count,
				table,		increment, 	&clock_tick	);

		update_disk_time( &disk_queue, increment );

		update_printer_queues( core, printers );	
		update_disk_queue_rr( &disk_queue, clock_tick );
		update_device_queues( core, table );//This is most certainly broken..
		//I am not sure if it is broken.. it Seems to never do anythnig
		//It appears that we are not able to have more than one process read
		//at once.
		
		reschedule_migrators_rr( core, process_list, process_count );

		update_core_instructions( core, clock_tick );
		execute_core_instructions( core, printers, table, &disk_queue, 0);
		
		context_switch_RR( core, clock_tick, increment );

       		print_cores( 	"CPU-RR<=>DISK-RR.txt",
				clock_tick, core,  process_list, process_count,
				printers,   table, disk_queue );

		//Check for completion
		complete = 0;
		for(i = 0; i < process_count; i++ )
		if( process_list[i].state != COMPLETE)  {complete = 1; break;}
	}

	//Output
	print_process_list( "CPU-RR<=>DISK-RR.txt", process_list, process_count );
	print_results( "CPU-RR<=>DISK-RR.txt", process_list, process_count );


	//*******************************
	//Cpu  RR
	//Disk EDF
	
	//Initialization
	clock_tick = 0;
	initialize_master_process_list( &process_list, &process_count, argc, arvg);	

	//IO Device initialization
	printers = malloc( sizeof(printer_block*));
	printers->printer1 = NULL;
	printers->printer2 = NULL;
	printers->print_queue_head = NULL;
	disk_queue = NULL;
	for( i = 0; i < 4; i++ ){
		core[i].running_process = NULL;
		core[i].process_queue_head = NULL;
		table[i].sem = 0;	//If all went well before, the tables should be
		table[i].wait_queue = NULL;	//ready but I leave these here for ensurance.
	}

	//Scheduling
	for( i = 0; i < process_count; i++ )
		schedule_process_rr( core, process_list + i );	
	
	print_process_list( "CPU-RR<=>DISK-EDF.txt", process_list, process_count );

	complete = 1;
	//Start clock
	while( complete != 0 ){

		//check for migration
		int increment = 0;
		get_increment(	core, 	 	process_list, 	process_count,
				QUANTUM, 	&increment, 	clock_tick	);

		update_time( 	core, 		process_list, 	process_count,
				table,		increment, 	&clock_tick	);

		update_disk_time( &disk_queue, increment );
		update_disk_queue( &disk_queue );


		update_printer_queues( core, printers );	
		update_device_queues( core, table );
		
		//general reschedule
		reschedule_migrators_rr( core, process_list, process_count );

		update_core_instructions( core, clock_tick );
		execute_core_instructions( core, printers, table, &disk_queue, 1);
		
		context_switch_RR( core, clock_tick, increment );

		/*print_process_list( "CPU-RR<=>DISK-EDF.txt", process_list, process_count );*/
		print_cores( 	"CPU-RR<=>DISK-EDF.txt",
			     	clock_tick, core,  process_list, process_count,
			     	printers,   table, disk_queue );

		//Check for completion
		complete = 0;
		for(i = 0; i < process_count; i++ )
		if( process_list[i].state != COMPLETE)  { complete = 1; break;}
	}


	print_process_list( "CPU-RR<=>DISK-EDF.txt", process_list, process_count );
	print_results( "CPU-RR<=>DISK-EDF.txt", process_list, process_count );
		

	//*******************************
	//Cpu  EDF
	//Disk RR

	//Initialization
	clock_tick = 0;
	initialize_master_process_list( &process_list, &process_count, argc, arvg);	

	//IO Device initialization
	printers->printer1 = NULL;
	printers->printer2 = NULL;
	printers->print_queue_head = NULL;
	for( i = 0; i < 4; i++ ){
		core[i].running_process = NULL;
		core[i].process_queue_head = NULL;
		table[i].sem = 0;	//If all went well before, the tables should be
		table[i].wait_queue = NULL;	//ready but I leave these here for insurance.
	}
	disk_queue = NULL;

	//Scheduling
	for( i = 0; i < process_count; i++ )
		schedule_process_edf( core, process_list + i );	
	
	print_process_list( "CPU-EDF<=>DISK-RR.txt", process_list, process_count );
	complete = 1;
	//Start clock
	while( complete != 0 ){

		int increment = 0;
		get_increment( 	core, 		process_list, 	process_count,
				0,	 	&increment, 	clock_tick	);

		update_time( 	core, 		process_list, 	process_count,
				table,		increment, 	&clock_tick	);

		update_disk_time( &disk_queue, increment );

		update_printer_queues( core, printers );	
		update_disk_queue_rr( &disk_queue, clock_tick );
		update_device_queues( core, table );//This is most certainly broken..
		//I am not sure if it is broken.. it Seems to never do anythnig
		//It appears that we are not able to have more than one process read
		//at once.
		
		reschedule_edf( core, process_list, process_count );//This is where preemption with the cpu occurs.

		update_core_instructions( core, clock_tick );
		execute_core_instructions( core, printers, table, &disk_queue, 0 );
		
		context_switch_edf( core, clock_tick, increment );

		/*print_process_list( "CPU-EDF<=>DISK-RR.txt", process_list, process_count );*/
		print_cores( 	"CPU-EDF<=>DISK-RR.txt",
				clock_tick, core,  process_list, process_count,
				printers,   table, disk_queue );

		//Check for completion
		complete = 0;
		for(i = 0; i < process_count; i++ )
		if( process_list[i].state != COMPLETE)  {complete = 1; break;}
	}

	//Output
	print_process_list( "CPU-EDF<=>DISK-RR.txt", process_list, process_count );
	print_results( "CPU-EDF<=>DISK-RR.txt", process_list, process_count );
	
	

	//*******************************
	//Cpu  EDF
	//Disk EDF

	//Initialization
	clock_tick = 0;
	initialize_master_process_list( &process_list, &process_count, argc, arvg);	

	//IO Device initialization
	printers->printer1 = NULL;
	printers->printer2 = NULL;
	printers->print_queue_head = NULL;
	for( i = 0; i < 4; i++ ){
		core[i].running_process = NULL;
		core[i].process_queue_head = NULL;
		table[i].sem = 0;	//If all went well before, the tables should be
		table[i].wait_queue = NULL;	//ready but I leave these here for insurance.
	}
	disk_queue = NULL;

	//Scheduling
	for( i = 0; i < process_count; i++ )
		schedule_process_edf( core, process_list + i );	

	print_process_list( "CPU-EDF<=>DISK-EDF.txt", process_list, process_count );
	
	complete = 1;
	complete = 1;
	//Start clock
	while( complete != 0 ){

		int increment = 0;
		get_increment( 	core, 		process_list, 	process_count,
				0,	 	&increment, 	clock_tick	);

		update_time( 	core, 		process_list, 	process_count,
				table,		increment, 	&clock_tick	);

		update_disk_time( &disk_queue, increment );

		update_disk_queue_rr( &disk_queue, increment );
		update_printer_queues( core, printers );	
		update_device_queues( core, table );
		
		reschedule_edf( core, process_list, process_count );//This is where preemption with the cpu occurs.

		update_core_instructions( core, clock_tick );
		execute_core_instructions( core, printers, table, &disk_queue, 0 );
		
		context_switch_edf( core, clock_tick, increment );

		/*print_process_list( "CPU-EDF<=>DISK-EDF.txt", process_list, process_count );*/
		print_cores( 	"CPU-EDF<=>DISK-EDF.txt",
				clock_tick, core,  process_list, process_count,
				printers,   table, disk_queue );

		//Check for completion
		complete = 0;
		for(i = 0; i < process_count; i++ )
		if( process_list[i].state != COMPLETE)  {complete = 1; break;}
	}
	

	//Output
	print_process_list( "CPU-EDF<=>DISK-EDF.txt", process_list, process_count );
	print_results( "CPU-EDF<=>DISK-EDF.txt", process_list, process_count );



//***************************************************
// Cleanup
	//There should be a free for ever allocation;
	free( process_list );
	free( printers );
}
