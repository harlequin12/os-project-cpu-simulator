//main_functions Function Definition File


/***************************************************************
 * Initialization
 *
 ***************************************************************
 */

void 	initialize_master_process_list( 
		process_control_block** process_list, 
		unsigned int* 		process_count, 
		int 			argNum, 
		char** 			mainArgs)
{
	   //Load contents of file into string
	   char* input_string = loadInputFileToString(argNum, mainArgs);
	   /*printf( "Input String contents:\n%s\n", input_string );*/
	   
	   //Read data from the input string to a process list
	   *process_count = initialize_process_list(process_list, input_string);
}
	
char* 	loadInputFileToString(int argNum, char** args)
{
	//open the file 
	FILE* input_file;
	if( argNum == 1 ) input_file = fopen("sampleInput.txt", "r");	
	else 		  input_file = fopen(args[1], "r");
	if( input_file == NULL ) ERROR("Error opening file.", 1);
	
  	// obtain file size:
	fseek( input_file, 0, SEEK_END);
	long lSize = ftell( input_file );
	rewind( input_file );

	// allocate memory to contain the whole file:
	char* input_string = (char*) malloc (sizeof(char)*lSize);
	if( input_string == NULL ) ERROR("Memory error", 2);

	// copy the file into input_string:
	size_t result = fread (input_string, 1, lSize, input_file);
	if( result != lSize ) ERROR("Reading error",3);

	//Cleanup and return
    	fclose( input_file );
	return input_string;
}

unsigned int  	initialize_process_list( 
			process_control_block** 
			process_list, 
			char* input_string )
{

	/*
 	* This function will read the instruction string and
	* create an array of processes initializing each PCB
	* correctly
	*
	* This function will return the number of elements in the array
	* 
 	*/

	unsigned int number_of_processes = 0;
	*process_list = malloc(0);

	unsigned int  process_number, deadline;
	unsigned char current_cpu, cpu_required;

	//loop through the string tokenizing with by C
	char* process_string;
	process_string = strtok(input_string, "C");
	do{
		//Trim leading white spaces
		while( isspace(*process_string) ) process_string++; 
		if(*process_string == 0) {/*puts("Empty String\n");*/ continue;}

		//Check to make sure that the process string only contains valid characters
		//RTDPFwr0123456789 \n\t
		size_t span = strspn(process_string, "RTDPFwr0123456789 \t\n"); //"\v\f\r"????
		if( strlen(process_string) != span ) {/*puts("contains invalid char");*/ continue;}

		//Load all of the first four numbers
		if( 0 == __get_first_four(process_string, &process_number, 
		&current_cpu, &cpu_required, &deadline) ) {/*puts("Failed to get numbers");*/ continue;}

		//locate first occurence of characer to find the process instruction string
		process_string = strpbrk(process_string, "RTDPF");	//Validation happens in function

		//Create a Queue from the instruction string
		instruction_node* queue_head = NULL;
		create_instruction_queue( &queue_head, process_string );
		if( queue_head == NULL ) {/*puts("Queue failure")*/;continue;}

		//Validate pid - must not already exist
		int i = 0;
		do	if( process_list[0][i].process_id == process_number ){i = -1; break;}
		while( ++i < number_of_processes );
		if( i == -1) {/*puts("processid already taken\n");*/ continue;}


		//Allocate space for new process and increment process counter
		number_of_processes++;
		*process_list = (process_control_block*) realloc( *process_list, sizeof(process_control_block)*number_of_processes);
		
		//initialize the new process in the correct place in the array
	 	init_PCB(( *process_list + number_of_processes-1 ), 
			 process_number, current_cpu, cpu_required, deadline, queue_head);
		

	}while( (process_string = strtok(NULL, "C") ) != NULL );
	
	free( process_string );
	free( input_string );

	return number_of_processes;
}

	int 	__get_first_four(
			char* process_string,  
			unsigned int *process_number, unsigned char *current_cpu, 
			unsigned char *cpu_required, unsigned int *deadline		)
	{
		*process_number = 0; *current_cpu = 0; *cpu_required = 0; *deadline = 0;
	
		//If four numbers are not found then it is invalid
		int scan_return = sscanf(process_string, "%u%hhu%hhu%u", 
			process_number, current_cpu, cpu_required, deadline);
		if( scan_return != 4) {puts("scan fail\n"); return 0; }
	
		//check to make sure these values are within their correct range.
		if( *process_number < 0 ) {/*puts("proc_id is negative");*/return 0;}
		if( *deadline	    < 0 ) {/*puts("deadline out range");*/ return 0;}
		if( *current_cpu   <= 0 || 
		    *current_cpu   >  4 ) {/*puts("c_cpu out range");*/	   return 0;}	
		if( *cpu_required  <  0 ||
	            *cpu_required   > 4 ) {/*puts("cpu_r out range");*/	   return 0;}  
			//1,2,3,4 are valid cpu nums
			//0,1,2,3,4 are valid required cpus

		return 1;
	}





/***********************************************************
 * Simulation
 *
 ***********************************************************
 */

void	get_increment(
		core_control_block*	core,	
		process_control_block*  process_list,
		unsigned int 		process_count,
		unsigned int		quantum,
		unsigned int* 		increment,
		unsigned int		clock_tick	)
{
	if( quantum <= 0 )	*increment = 0-1; //The largest number that increment can be.
	else			*increment = QUANTUM - ((clock_tick)%QUANTUM);
	int i;
	for( i = 0; i < process_count; i++ ){
		if( process_list[i].state != COMPLETE &&
		    process_list[i].state != READY &&
		    process_list[i].state != WAIT_SEMAPHORE &&
		    process_list[i].state != WAIT_PRINTER )
			//In order to count the process currently running on the disk, 
			// we would have to either search for the element with the earliers deadline or
			// use the disk queue as is apropriate to the scheduling schema.
			//It is easier to simply scan all of the processes that are waiting on the disk
			// at the minor risk of letting the increment be
			// lesser than it needs to be to insure that a cs will occur.
			//In the case where this happens, the rest of the cylce will see nothing happen, 
			// since all of the processes remaining_intsruction_time's will remain positive,
			// and the next cycle should cover the remainder of the time.
		{
			if( process_list[i].remaining_instruction_time < *increment )
				*increment = process_list[i].remaining_instruction_time;
		}
	}
	//Check for migrators - a process that needs to move to another cpu
	//if there is one, make the increment 0 so that the CS can happen
	for( i = 0; i < 4; i++ )
		if( core[i].running_process != NULL )
		if( core[i].running_process->required_cpu != 0 &&
	      	    core[i].running_process->current_cpu != core[i].running_process->required_cpu ){
			*increment = 0; 
			break;
		}
}

void	update_time(
		core_control_block*	core,	
		process_control_block*  process_list,
		unsigned int 		process_count,
		table_block*		table,
		unsigned int 		increment,
		unsigned int* 		clock_tick	)
{
	// Cores, DVDs, Tables and Migrators
	int i;
	process_control_block*  process;
	for( i = 0; i < process_count; i++ ){
		process = process_list + i;

		// DVD
		if( process->state == WAIT_DVD ){
			//It is a little easier to update dvd status here rather than doing it seperately later 
			process->remaining_instruction_time -= increment;
			if( process->remaining_instruction_time == 0){
				process->state = MIGRATING;
			}
			continue;
		}

		// Tables
		if( process->state == READ_TABLE_1 || process->state == READ_TABLE_2 || 
		    process->state == READ_TABLE_3 || process->state == READ_TABLE_4 )
		{
			process->remaining_instruction_time -= increment;
			//Again, it is faster and simpler to update the table status in addition to it's time
			if( process->remaining_instruction_time == 0 ){
				table[process->state - 7].sem++; //Signal
				process->state = MIGRATING;
			}
			continue;
		}
		if( process->state == WRITE_TABLE_1 || process->state == WRITE_TABLE_2 ||
		    process->state == WRITE_TABLE_3 || process->state == WRITE_TABLE_4 )
		{
			process->remaining_instruction_time -= increment;
			//Again, it is faster and simpler to update the table status in addition to it's time
			if( process->remaining_instruction_time == 0 ){
				table[process->state - 11].sem++; //Signal
				process->state = MIGRATING;
			}
			continue;
		}

		if( process->state == RUNNING ||
		    process->state == MIGRATING ||
		    process->state == PRINTING )
			process->remaining_instruction_time -= increment;
			//Processes in these states will have their state's updated in the functions following this one
	}

	//Clock update
	*clock_tick += increment;
}

void 	update_device_queues( 	
		core_control_block* 	core, 
		table_block*		table		)
{
	//Cores, DVD, and printers are updated in update_time.
	//
	//Table
	int i;
	for( i = 0; i < 4; i++ ){
		if( table[i].wait_queue != NULL )
		if( table[i].sem == 0 ){
			if( table[i].wait_queue->process->current_instruction == TABLE_WRITE_1 + i ){	//next instruction is a write
				table[i].wait_queue->process->state = WRITE_TABLE_1 + i;
				table[i].sem--; //Wait
				table[i].wait_queue = table[i].wait_queue->next;
			} else
			if( table[i].wait_queue->process->current_instruction == TABLE_READ_1 + i){	//next instruction is a read.
				while( table[i].wait_queue != NULL ){
					if( table[i].wait_queue->process->current_instruction == TABLE_READ_1 + i ){
						table[i].wait_queue->process->state = WRITE_TABLE_1 + i;
						table[i].sem--; //Wait
					}else break;
					table[i].wait_queue = table[i].wait_queue->next;
				}
			}

		}
	}
}

void 	reschedule_migrators_rr( core_control_block* core, process_control_block* process_list, unsigned int process_count ){
	int i;
	for( i = 0; i < process_count; i++ )
		if( process_list[i].state == MIGRATING && process_list[i].remaining_instruction_time == 0 ){
			process_list[i].state = READY;
			if( process_list[i].required_cpu != 0 )
				process_list[i].current_cpu = process_list[i].required_cpu;
			schedule_process_rr( core, process_list + i );
		}
}

void 	reschedule_edf( core_control_block* core, process_control_block* process_list, unsigned int process_count ){
	int i;
	for( i = 0; i < process_count; i++ )
		if( process_list[i].state == MIGRATING && process_list[i].remaining_instruction_time == 0 ){
			process_list[i].state = READY;
			if( process_list[i].required_cpu != 0 )
				process_list[i].current_cpu = process_list[i].required_cpu;
			schedule_process_edf( core, process_list + i );
		}
}

void 	update_core_instructions( core_control_block* core, unsigned int clock_tick )
{
	//If a core has a process and it has 0 remaining time
	//get the next instruction and act on it.
	int i;
	process_control_block* proc;
	
	for( i = 0; i < 4; i++ ){
		proc = core[i].running_process;
		if( proc != NULL )
		if( proc->remaining_instruction_time == 0)
			if( proc->instruction_queue_head != NULL ){
				proc->current_instruction = proc->instruction_queue_head->instruction;
				proc->remaining_instruction_time = proc->instruction_queue_head->time;

				//Pop the instruction queue
				instruction_node* temp       = proc->instruction_queue_head;
				proc->instruction_queue_head = proc->instruction_queue_head->next;
				free( temp );

			}else {
				//process is complete
				proc->state = COMPLETE;
				proc->current_instruction = 0;
				proc->complete_time = clock_tick;	
			}
	}
}

void 	execute_core_instructions( 
		core_control_block* 	core, 
		printer_block* 		printers,
		table_block*		table,
		process_queue_node**	disk_queue,
		unsigned char		sched_scheme	)	//0 = RR, 1 = EDF
{
	int i, j;
	process_control_block* process;
	//This loop here is where necessity for a migration will be detected.
	for( i = 0; i < 4; i++ ){
		process = core[i].running_process;

		if( process != NULL ){
		if( process->required_cpu != 0 && process->current_cpu != process->required_cpu ){
			process->state = MIGRATING;
			process->current_cpu = process->required_cpu;

			//push migration time
			instruction_node* temp = malloc( sizeof(instruction_node*) );
			temp->instruction = process->current_instruction;
			temp->time 	  = process->remaining_instruction_time;
			temp->next 	  = process->instruction_queue_head;

			process->instruction_queue_head = temp;
			process->current_instruction = RUN;
			process->remaining_instruction_time = NETWORK_DELAY;

			continue;
		}
		if( process->state == RUNNING )
		switch( process->current_instruction ) {
			case RUN:
				process->state = RUNNING;	break;

			case DVD_IO:
				process->state = WAIT_DVD;	break;

			case HARD_DISK_IO:
				process->state = WAIT_DISK;
				if( sched_scheme == 0 )	//RR
					schedule_disk( disk_queue, process, 0 );	//When the deadline is 0, it will be placed on the end
				else 
					schedule_disk( disk_queue, process, process->deadline );	//EDF
				break;

			case PRINTER_REQUEST:
				process->state = WAIT_PRINTER;
				enqueue_process( &(printers->print_queue_head), process );
				break;
				
			case TABLE_WRITE_1:
			case TABLE_WRITE_2:
			case TABLE_WRITE_3:
			case TABLE_WRITE_4:
				j = process->current_instruction - 4;
				if( table[j].sem == 0 )	{
					process->state = WRITE_TABLE_1 + j;
					table[j].sem--; //wait
				}else{
					process->state = WAIT_SEMAPHORE;
					enqueue_process( &(table[j].wait_queue), process );
				}
				break;

			case TABLE_READ_1:
			case TABLE_READ_2:
			case TABLE_READ_3:
			case TABLE_READ_4:
				j = process->current_instruction - 8;
				if( table[j].wait_queue == NULL ){	
					process->state = READ_TABLE_1 + j;
					table[j].sem--; //wait
				}else{
					process->state = WAIT_SEMAPHORE;
					enqueue_process( &(table[j].wait_queue), process );
				}
				break;

			default: printf("default attained%i\n", process->current_instruction); break;
		}
		//End of instruction switch
	}
		//End of NULL if 
	}
		//End of loop
}

void	context_switch_RR( core_control_block* core, unsigned int clock_tick, unsigned int increment )
{
	//When to do a CS
	//-When the quanta is hit
	//-when running process is null and process queue is not
	//-when process is complete
	//-when a process is blocked and not null
	//-When a process has started to migrate

	int i;
	process_control_block*	process;
	for( i = 0; i < 4; i++){
		process = core[i].running_process;

		//Were getting extra context switches happening here when migration is happening
		if( process != NULL){
			if( increment == 0 ){	//This case covers Context Switches for migrating processe
				if( core[i].running_process->state == MIGRATING ){
					if(core[i].process_queue_head != NULL){
						core[i].running_process = core[i].process_queue_head->process;
						core[i].running_process->state = RUNNING;
						core[i].process_queue_head = core[i].process_queue_head->next;
					}else core[i].running_process = NULL;
					continue;
				}
			}else {	//General CS only happens after some time has passed
				if( 
				    core[i].running_process->state != RUNNING	     	&&
				    core[i].running_process->state != READY	     	)
				    /*core[i].running_process->state != MIGRATING	     	)*/
				{
					if(core[i].process_queue_head != NULL){
						core[i].running_process = core[i].process_queue_head->process;
						core[i].running_process->state = RUNNING;
						core[i].process_queue_head = core[i].process_queue_head->next;
					}else core[i].running_process = NULL;
					continue;
				}
				if(  (clock_tick%QUANTUM) == 0 ){
					core[i].running_process->state = READY;
					enqueue_process( &(core[i].process_queue_head), core[i].running_process );
					core[i].running_process = core[i].process_queue_head->process;
					core[i].running_process->state = RUNNING;
					if(core[i].process_queue_head != NULL)
						core[i].process_queue_head = core[i].process_queue_head->next;
					continue;
				} 
			}
		}
		if( core[i].running_process == NULL && core[i].process_queue_head != NULL ){
			//If there is no running process and there is something in the queue
			//then take it and execute it
			core[i].running_process = core[i].process_queue_head->process;
			core[i].running_process->state = RUNNING;
			continue;
		}
	}
}

void	context_switch_edf( core_control_block* core, unsigned int clock_tick, unsigned int increment ){

	//When to do a context switch.
	//-When a migration is necessary
	//-When a process is complete
	//-When a process is blocked
	//-When it is NULL
	//-After time has been updated and there are processes available that 
	// have sooner deadlines than that of the process in the each core.
	int i;
	process_control_block*	process;
	for( i = 0; i < 4; i++){
		process = core[i].running_process;

		//Were getting extra context switches happening here when migration is happening
		if( process != NULL){
			if( increment == 0 ){	//This case covers C Switches for migrating processe
				if( core[i].running_process->state == MIGRATING ){
					if(core[i].process_queue_head != NULL){
						core[i].running_process = core[i].process_queue_head->process;
						core[i].running_process->state = RUNNING;
						core[i].process_queue_head = core[i].process_queue_head->next;
					}else core[i].running_process = NULL;
					continue;
				}
				
//The increment is 0 so the cs is not happening.


			}else {	//General CS only happens after some time has passed
				if( 
				    core[i].running_process->state != RUNNING	     	&&
				    core[i].running_process->state != READY	     	)
				{
					if(core[i].process_queue_head != NULL){
						core[i].running_process = core[i].process_queue_head->process;
						core[i].running_process->state = RUNNING;
						core[i].process_queue_head = core[i].process_queue_head->next;
					}else	core[i].running_process = NULL;
					continue;
				}
			}
		}
		if( core[i].running_process == NULL && core[i].process_queue_head != NULL ){
			//If there is no running process and there is something in the queue
			//then take it and execute it
			core[i].running_process = core[i].process_queue_head->process;
			core[i].process_queue_head = core[i].process_queue_head->next;
			core[i].running_process->state = RUNNING;

			continue;
		}
	}
}







/*****************************************************
 * Output
 *
 *****************************************************
 */



void 	print_process_list( 
		const char*		file_name,
		process_control_block* 	process_list, 
		unsigned int 		number_of_processes )
{
	static char* inst_strings[12] = 
		{"R","T","D","P","F1w","F2w","F3w","F4w","F1r","F2r","F3r","F4r"};	
	static char* state_strings[ ] = 
	 	{"COMPLETE", 	"RUNNING", 	"READY", "WAIT_DISK", 
		 "WAIT_DVD", 	"WAIT_PRINTER", "WAIT_SEMAPHORE", 
		 "READ_TABLE_1",  "READ_TABLE_2",  "READ_TABLE_3",  "READ_TABLE_4",
		 "WRITE_TABLE_1", "WRITE_TABLE_2", "WRITE_TABLE_3", "WRITE_TABLE_4",
		 "PRINTING",	  "MIGRATING"};
	FILE	*outfile;
	outfile = fopen( file_name, "a+" );

	fputs("Process List:\nID  State            Cur CPU  Req CPU  DLine  Complete Time Instruction List", outfile );
	/*puts("Process List:\nID State          Cur CPU  Req CPU  DLine  Complete Time Instruction List" );*/
	 
	int i = 0;
	while(  i < number_of_processes){
		fprintf(outfile, "\n%-3u %-16s %-8hhu %-7hhu %-6u %-13u %-3s %-3u (",
		/*printf( "\n%-3u %-14s %-8hhu %-7hhu %-6u %-13u %-3s %-3u (",*/
				process_list[i].process_id,
				state_strings[process_list[i].state],
				process_list[i].current_cpu,
				process_list[i].required_cpu,
				process_list[i].deadline,
				process_list[i].complete_time,
				inst_strings[process_list[i].current_instruction],
				process_list[i].remaining_instruction_time);
		instruction_node* tracer = process_list[i].instruction_queue_head;
		while( tracer != NULL ){
			fprintf(outfile, "%s %u ", inst_strings[tracer->instruction], tracer->time);
			/*printf( "%s %u ", inst_strings[tracer->instruction], tracer->time);*/
	/*puts("hi");*/
			tracer = tracer->next;
		} fprintf(outfile, ")");
		/*} printf( ")");*/
		i++;
	}
	fputs("\n\n", outfile);
	/*puts("\n\n");*/
	fclose(outfile);
}

void 	print_queue( FILE* outfile, process_queue_node* queue ){
	process_queue_node* tracer = queue;
	if( tracer != NULL ){
		fprintf(outfile, "[");
		while( tracer->next != NULL ){
			fprintf(outfile, "%u, ", tracer->process->process_id );
			tracer = tracer->next;
		} fprintf(outfile, "%i]\n", tracer->process->process_id);
	}else fprintf(outfile, "[]\n");
}

void 	print_results( const char* filename, process_control_block* process_list, unsigned int process_count ){

	FILE *outfile;
	outfile = fopen( filename, "a+" );
	int i, missed_deadlines = 0;
	fprintf( outfile, "\n\nTotal number of processes: %u\n", process_count );
	fprintf( outfile, "Process IDs of late processes: ");
	for( i = 0; i < process_count; i++ )
		if( process_list[i].deadline != 0 && process_list[i].complete_time > process_list[i].deadline ){
			missed_deadlines++;
			fprintf(outfile, "%u, ", process_list[i].process_id );
		}

	fprintf( outfile, "\nNumber of processes missing their deadline: %i\n", missed_deadlines );
	fclose( outfile );
}

void 	print_cores( 
		const char*		filename,
		unsigned int 		clock_tick, 
		core_control_block* 	core, 
		process_control_block*	process_list, 
		unsigned int 		process_count, 
		printer_block* 		printers, 
		table_block* 		table,  
		process_queue_node* 	disk_queue)
{
	FILE	*outfile;
	outfile = fopen( filename, "a+");
	//Clock time 
	fprintf( outfile, "<<<Clock tick: %u>>>\n", clock_tick);
	//CPUs
	int i, j;
	for( i = 0; i < 4; i++ ){
		fprintf( outfile, "Core %i: ", i+1);
		if( core[i].running_process != NULL )
			fprintf( outfile, "%-2i ", core[i].running_process->process_id);
			print_queue( outfile, core[i].process_queue_head );
	}
	//Disk
	fprintf( outfile, "Disk Queue: ");
	print_queue( outfile, disk_queue );
	//DVDs
	fprintf( outfile, "DVDs: ");
	for( i = 0; i < process_count; i++ ){
		if( process_list[i].state == WAIT_DVD )
			fprintf( outfile, "%3u ", process_list[i].process_id);
	}fputs("\n", outfile );
	//Printers
	fprintf( outfile, "Printer 1:");
	if( printers->printer1 != NULL)
	fprintf( outfile, "%-3u", printers->printer1->process_id);
	fprintf( outfile, "\nPrinter 2:");
	if( printers->printer2 != NULL)
	fprintf( outfile, "%-3u", printers->printer2->process_id);
	fprintf( outfile, "\nPrinter Queue: ");
	/*print_queue( filename, printers->print_queue_head );*/
	//Table
	for( i = 0; i < 4; i++ ){
		fprintf( outfile, "Table %i: ", i + 1);
		for( j = 0; j < process_count; j++ )
			if( process_list[j].state == READ_TABLE_1 + i )
				fprintf( outfile, "R%u ", process_list[i].process_id);
		for( j = 0; j < process_count; j++ )
			if( process_list[j].state == WRITE_TABLE_1 + i )
				fprintf( outfile, "W%u ", process_list[i].process_id);

		process_queue_node* tracer = table[i].wait_queue;
		if( tracer != NULL ){
			fprintf( outfile, "[");
			while( tracer->next != NULL ){
				if( tracer->process->current_instruction == TABLE_WRITE_1 + i)
					fprintf( outfile, "W%u, ", tracer->process->process_id );
				else
				if( tracer->process->current_instruction == TABLE_READ_1 + i)
					fprintf( outfile, "R%u, ", tracer->process->process_id );
				else fprintf( outfile, "X_X, ");
				tracer = tracer->next;
			} 
			if( tracer->process->current_instruction == TABLE_WRITE_1 + i)
				fprintf( outfile, "W%u]\n", tracer->process->process_id );
			else
			if( tracer->process->current_instruction == TABLE_READ_1 + i)
				fprintf( outfile, "R%u]\n", tracer->process->process_id );
			else fprintf( outfile, "X_X, ");
		}else fprintf( outfile, "[]\n");
		fprintf( outfile, "Sem %i:%i\n", i + 1, table[i].sem);
	}

	fputs("\n", outfile );
	fclose( outfile );

}



		
