//IO Device Definition File



/*********************************************************
 * Disk structure
 *
 *********************************************************
 */



void 	schedule_disk(
		process_queue_node**	disk_queue,
		process_control_block*	process,
	       	unsigned int		deadl		)//This should always either be 0 if we are using RR
{                                                        //or the process deadline if it is to be edf
	if( *disk_queue == NULL ){
		//If there is nothing on the core, then
		//simply place the process on the running process pointer
		*disk_queue = malloc( sizeof (process_queue_node* ));
		( *disk_queue )->process = process;
		( *disk_queue )->next = NULL;
	}else
	if( deadl == 0 ){
		//If the process has no dead line, then 
		//simply add the process to the end of the queue
		enqueue_process( disk_queue, process );
	}else{
		if( process->deadline < ( *disk_queue )->process->deadline ||
		    ( *disk_queue )->process->deadline == 0 ){
			//If the running process has a longer deadline, then
			//prempt it!!
			process_control_block* temp = ( *disk_queue )->process;
			( *disk_queue )->process = process;
			schedule_disk( disk_queue, temp, temp->deadline );
			//Here, I use recursive call to reschedule the
			//preempted process.
		}else{	
			//Otherwise, add the process in its right place on the queue
			process_queue_node** tracer = disk_queue;
	
			while( *tracer != NULL ){
				if(( (*tracer)->process->deadline != 0 &&
				     (*tracer)->process->deadline > process->deadline ) ||
				     (*tracer)->process->deadline == 0			)
					break;
				tracer = &( (*tracer)->next );
			}
			process_queue_node* new_node = malloc( sizeof( process_queue_node* ) );
			new_node->next = (*tracer); 
			new_node->process = process;
			*tracer = new_node;
		}
	}
}

void	update_disk_time(process_queue_node** disk_queue, unsigned int increment ){
	if( ( *disk_queue ) != NULL )
		( *disk_queue )->process->remaining_instruction_time -= increment;
}

void update_disk_queue( process_queue_node** disk_queue ){
	if( *disk_queue != NULL)
	if( disk_queue[0]->process->remaining_instruction_time == 0){
		disk_queue[0]->process->state = MIGRATING;
		disk_queue[0] = disk_queue[0]->next;
	}
}

void	update_disk_queue_rr( process_queue_node** disk_queue, unsigned int clock_tick ){
	
	if( *disk_queue != NULL )
	if( clock_tick%QUANTUM == 0 || disk_queue[0]->process->remaining_instruction_time == 0)
		if( disk_queue[0]->process->remaining_instruction_time == 0 ){
			disk_queue[0]->process->state = MIGRATING;
			disk_queue[0] = disk_queue[0]->next;
		}else{
			enqueue_process( disk_queue, disk_queue[0]->process );
			disk_queue[0] = disk_queue[0]->next;
		}
}


/*********************************************************
 * DVD structure
 *
 *********************************************************
 */


/*********************************************************
 * Printer Structure
 *
 *********************************************************
 */

struct printer_block {
	//Non Preemtive!
	
	process_control_block* printer1;
	process_control_block* printer2;

	//Printing Queue
	process_queue_node* print_queue_head;	
};

typedef struct printer_block printer_block;

void	update_printer_queues(core_control_block* core, printer_block* printers )
{
	//if a process is done printing, then rescedule it
	if( printers->printer1 != NULL )
	if( printers->printer1->remaining_instruction_time == 0 ){
		printers->printer1->state = MIGRATING;
		printers->printer1 = NULL;
		//If there is another process waiting in the queue then put it in
		if( printers->print_queue_head != NULL ){
			printers->printer1 = printers->print_queue_head->process;
			printers->printer1->state = PRINTING;	
			printers->print_queue_head = printers->print_queue_head->next;
		}
	}
	if( printers->printer2 != NULL )
	if( printers->printer2->remaining_instruction_time == 0 ){
		printers->printer2->state = MIGRATING;
		printers->printer2 = NULL;
		//If there is another process waiting in the queue then put it in
		if( printers->print_queue_head != NULL ){
			printers->printer2 = printers->print_queue_head->process;
			printers->printer2->state = PRINTING;	
			printers->print_queue_head = printers->print_queue_head->next;
		}
	}
	//If there is anything in the queue and nothing in a printer then putit in the printere
	if( printers->printer1 == NULL && printers->print_queue_head != NULL){
	
		printers->printer1 = printers->print_queue_head->process;
		printers->printer1->state = PRINTING;
		printers->print_queue_head = printers->print_queue_head->next;
	}
	if( printers->printer2 == NULL && printers->print_queue_head != NULL){
	
		printers->printer2 = printers->print_queue_head->process;
		printers->printer2->state = PRINTING;
		printers->print_queue_head = printers->print_queue_head->next;
	}
}		




/*********************************************************
 * Table structure
 *
 *********************************************************
 */

struct table_block {
	
	int 			sem;
	process_queue_node*	wait_queue;

};

typedef struct table_block table_block;



