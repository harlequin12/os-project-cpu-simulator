//Process Control Block Definition File 




/******************************************************* 
 * Data Structure Definitions
 *
 *******************************************************
 */


// Process State Type
enum process_state
{
	COMPLETE	= 0,
	RUNNING 	= 1,
	READY 		= 2,		
	WAIT_DISK 	= 3,	//3
	WAIT_DVD 	= 4,
	WAIT_PRINTER 	= 5,
	WAIT_SEMAPHORE 	= 6,
	READ_TABLE_1 	= 7,	//7
	READ_TABLE_2 	= 8,
	READ_TABLE_3 	= 9,
	READ_TABLE_4 	= 10,
	WRITE_TABLE_1 	= 11,
	WRITE_TABLE_2 	= 12,
	WRITE_TABLE_3 	= 13,
	WRITE_TABLE_4 	= 14,
	PRINTING 	= 15,
	MIGRATING	= 16	//16
};

// Instruction Type
enum instruction_type
{
	RUN		= 0,		//0	//R 	Run, 
	DVD_IO 		= 1,		//1	//T 	DVD I/O request, 
	HARD_DISK_IO 	= 2,	//2	//D 	hard Disk I/O request, 
	PRINTER_REQUEST = 3,//3	//P 	printer request, 
	TABLE_WRITE_1 	= 4,	//4	//Fir 	read table i, i=1,2,3,4. 
	TABLE_WRITE_2 	= 5,  //5
	TABLE_WRITE_3 	= 6,  //6
	TABLE_WRITE_4 	= 7,  //7
	TABLE_READ_1 	= 8,	//8	//Fiw 	write table i, i=1,2,3,4. 
	TABLE_READ_2 	= 9,   //9
	TABLE_READ_3 	= 10,	//10
	TABLE_READ_4 	= 11	//11
};


//Instruction Queue structure
struct instruction_node
{
	instruction_type 	 instruction;
	unsigned int 		 time;
	struct instruction_node* next;
};

//PCB Structure
struct process_control_block
{

	unsigned int  	process_id;		//Running
	unsigned char 	current_cpu;		//Ready
	unsigned char 	required_cpu;		//DISK_WAIT
	unsigned int  	deadline;		//DVD_WAIT
	unsigned int	complete_time; 		//PRINTER_WAIT			
						//SEMAPHORE_WAIT
	process_state 	state;			//COMPLETED

	instruction_type	current_instruction;
	unsigned int 		remaining_instruction_time;	

	instruction_node*	instruction_queue_head;
};




/*******************************************************
 * Process Control Block Operations
 *
 *******************************************************
 */


void	init_PCB( 
		process_control_block* pcb,
		unsigned int 	pid, 
		unsigned char 	ccpu, 
	        unsigned char 	rcpu,
		unsigned int	dline,
		instruction_node*	instructionQHead	)
{
	//Number variables	
	pcb->process_id    = pid;
	pcb->current_cpu   = ccpu;
	pcb->required_cpu  = rcpu;
	pcb->deadline      = dline;
	pcb->complete_time = 0;

	//Current Instruction
	pcb->current_instruction 	= instructionQHead->instruction;
	pcb->remaining_instruction_time = instructionQHead->time;

	//Pop the top instruction off of the queue. It's values are loaded above.
	instruction_node* temp = instructionQHead;
	pcb->instruction_queue_head = instructionQHead->next;
	free( instructionQHead );

	//Process State
	pcb->state = READY;
}

/******************************************************************
 *Instruction Queue Operations
 *
 ******************************************************************
 */


void	create_instruction_queue(instruction_node** queue_head, char* instruction_string )
{

	//Remember: if any thing about this is invalid, the process should be discarded
	while( instruction_string != NULL ){
		
		//Get the instruction first
		//At this point we may assume that the first char is valid
		instruction_type instruction;

			if( *instruction_string == 'R' ) instruction = RUN;
		else	if( *instruction_string == 'T' ) instruction = DVD_IO; 
		else	if( *instruction_string == 'D' ) instruction = HARD_DISK_IO;
		else	if( *instruction_string == 'P' ) instruction = PRINTER_REQUEST;
		else	if( *instruction_string == 'F' ){

				if( instruction_string[1] == '1' && instruction_string[2] == 'r') instruction = TABLE_READ_1;
			else	if( instruction_string[1] == '2' && instruction_string[2] == 'r') instruction = TABLE_READ_2;
			else 	if( instruction_string[1] == '3' && instruction_string[2] == 'r') instruction = TABLE_READ_3;
			else 	if( instruction_string[1] == '4' && instruction_string[2] == 'r') instruction = TABLE_READ_4;
			else 	if( instruction_string[1] == '1' && instruction_string[2] == 'w') instruction = TABLE_WRITE_1;
			else 	if( instruction_string[1] == '2' && instruction_string[2] == 'w') instruction = TABLE_WRITE_2;
			else 	if( instruction_string[1] == '3' && instruction_string[2] == 'w') instruction = TABLE_WRITE_3;
			else 	if( instruction_string[1] == '4' && instruction_string[2] == 'w') instruction = TABLE_WRITE_4;
			
			else	{*queue_head = NULL; break;}
		}
		else {puts("instruction not found\n"); *queue_head = NULL; break;}


		//Skip over White spaces
		while( 0 == isspace(*instruction_string) ) instruction_string++;
		
		//Get the time
		unsigned int time;
		int scan_return = sscanf( instruction_string, " %i", &time );
		/*printf("Scan Return:%i\nTime:%i\n", scan_return, time);*/

		//Check that time is valid
		if( scan_return != 1 ) 	{ /*puts("Scan return invalid\n");*/	*queue_head = NULL; break; }		
		if( time < 0)		{ /*puts("time invalid\n");*/		*queue_head = NULL; break; }		

		//enqueue
		/*printf("Attempting to enqueueing instruction:%i Time:%i\n", instruction, time);*/
		enqueue_instruction(queue_head, instruction, time);

		//Incrementer
		instruction_string = strpbrk(instruction_string+1, "RTDPF");
	}
}

void	enqueue_instruction( instruction_node** queue_head, instruction_type instruct, unsigned int tm )
{
	struct instruction_node* node_tracer;
	if( *queue_head == NULL ){
		*queue_head = malloc(sizeof( instruction_node ));
		if( *queue_head == NULL ) ERROR("Malloc error enqueue instruction\n", 1);
		node_tracer = *queue_head;
	}else{
		node_tracer = *queue_head;
		while( node_tracer->next != NULL )
			node_tracer = node_tracer->next;
		node_tracer->next = malloc(sizeof( instruction_node ));
		if( *queue_head == NULL ) ERROR("Malloc error enqueue instruction\n", 1);
		node_tracer = node_tracer->next;
	}
	if( node_tracer == NULL ) ERROR("Malloc error in enqueue_instruction", 1);

	node_tracer->instruction = instruct;
	node_tracer->time = tm;
	node_tracer->next = NULL;
}
