//Core Control Block Definition File



/**************************************************************
 * Data Structures
 *
 *************************************************************
 */


	// Process Queue Data Structure
	struct process_queue_node
	{
		process_control_block*	process;
		process_queue_node* 	next;
	};
	
	// Core Control Block Data Structure
	struct core_control_block 
	{
		//Running Process
		process_control_block* 	running_process;
	
		//Process queue
		process_queue_node*	process_queue_head;
	};


/***************************************************************
 * Core Operations
 *
 *************************************************************** 
 */

void	schedule_process_rr(
		core_control_block*	core,
		process_control_block*	process ) 
{
	core_control_block* cr = core + process->current_cpu - 1;
	enqueue_process( &(cr->process_queue_head), process );

	if( cr->running_process == NULL ){	//Since we just enqueue, the queue isn'tnull
		cr->running_process = cr->process_queue_head->process;
		cr->running_process->state = RUNNING;
		cr->process_queue_head = cr->process_queue_head->next;
	}
}

void 	schedule_process_edf(
		core_control_block*	core,
		process_control_block*	process )
{
	core_control_block* cr = core + process->current_cpu - 1;


	/*printf("Bfore Core %hu: ",  process->current_cpu);*/
	/*if( cr->running_process != NULL){*/
	/*printf("%u  ", cr->running_process->process_id);*/
	/*process_queue_node* tracer = cr->process_queue_head;*/
	/*if( tracer != NULL ){*/
		/*printf("[");*/
		/*while( tracer->next != NULL ){*/
			/*printf( "%u, ", tracer->process->process_id);*/
			/*tracer = tracer->next;*/
		/*}*/
		/*printf("%u]\n", tracer->process->process_id);*/
	/*}else puts("[]");*/
	/*}else printf(" []\n");*/


	if( cr->running_process == NULL ){
		//If there is nothing on the core, then
		//simply place the process on the running process pointer
		cr->running_process = process;
		process->state = RUNNING;
	}else
	if( process->deadline == 0 ){
		//If the process has no dead line, then 
		//simply add the process to the end of the queue
		if(cr->running_process->process_id != process->process_id)
			enqueue_process( &( cr->process_queue_head), process );
		
		/*schedule_process_rr( core, process );*/
	}else{
		if( process->deadline < cr->running_process->deadline ||
		    cr->running_process->deadline == 0 ){
			//If the running process has a longer deadline, then
			//prempt it!!
			//
			//Could it be here!!??
			if( cr->running_process->process_id != process->process_id )
			{
				process_control_block* temp = cr->running_process;
				cr->running_process = process;
				process->state = RUNNING;
				temp->state = READY; 
				schedule_process_edf( core, temp);
			}
			//Here, I use recursive call to reschedule the
			//preempted process.
		}else{	
			//Otherwise, add the process in its right place on the queue
			process_queue_node** tracer = &( cr->process_queue_head );
	
			while( *tracer != NULL ){
				if(( (*tracer)->process->deadline != 0 &&
				     (*tracer)->process->deadline > process->deadline ) ||
				     (*tracer)->process->deadline == 0			)
					break;
				tracer = &( (*tracer)->next );
			}
			process_queue_node* new_node = malloc( sizeof( process_queue_node* ) );
			new_node->next = (*tracer); 
			new_node->process = process;
			if( *tracer == NULL )	{
				*tracer = new_node;
			}else
			if( (*tracer)->process->process_id != process->process_id){
				*tracer = new_node;
			}
		}
	}
}


/**************************************************************
 * Process Queue Operations
 *
 **************************************************************
 */

void	enqueue_process( process_queue_node** queue_head, process_control_block* proc )
{
	process_queue_node* tracer;
	if( *queue_head == NULL ){
		*queue_head = malloc( sizeof(process_queue_node ));
		if( *queue_head == NULL ) ERROR("Malloc fail enqueue process", 1);
		tracer = *queue_head;
	}else{
		tracer = *queue_head;
		while( tracer->next != NULL )
			tracer = tracer->next;
		tracer->next = malloc( sizeof(process_queue_node ));
		if( tracer->next == NULL ) ERROR("Malloc fail enqueue process", 1);
		tracer = tracer->next;
	}

	tracer->process = proc;
	tracer->next = NULL;
}




