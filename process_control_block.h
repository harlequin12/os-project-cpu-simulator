//Process Conrol Block Header File



/*****************************************************
 *Data Structure Definitions
 * 
 ******************************************************
 */
	// Process State Type
	typedef enum 	process_state		process_state;	

	// Instruction Type Definition
	typedef enum 	instruction_type	instruction_type;	
		/* This type will be used to indicate the instructions
		 * that given instruction string will carry
		 *
		 */

	// PCB Structure
	typedef struct 	process_control_block 	process_control_block;
	
	// Instruction Queue Structure
	typedef struct 	instruction_node 	instruction_node;
		/* This will provide a queue to the process_control_block
		 * to hold the instrictions given to it. 
		 */


/******************************************************
 *Process Control Block Operations
 *
 ******************************************************
 */
void init_PCB(
		process_control_block* pcb,
		unsigned int 	pid,
		unsigned char 	ccpu,
		unsigned char	rcpu,
		unsigned int	dline,
		instruction_node*	instructionQHead	);

	/* This function's purpose is to simply initialize the PCB with the initial values given to it.
	 * It takes the first instruction and time off of the instuction queue,
	 * pops the queue and then initializes current_instruction and remaining_ins.._time with them.
	 * Process state it set to READY, preparing it to be scheduled.
	 * 
	 */

/*******************************************************
 *Instruction Queue Operations
 *
 *******************************************************
 */
void	create_instruction_queue( 
		instruction_node** 	ptr_to_queue_head, 
		char* 			instruction_string);
	/* The above function takes a string, that it assumes starts with a valid instruction string, and reads the input into an instruction_queue that is given to it.
	 * If it detects any invalidity it will return a NULL pointer to indicate the failure.
	 *
	 */

void 	enqueue_instruction(
		instruction_node** 	queue_head,
		instruction_type	instruct,
		unsigned int		tm)		;
	/* This function accepts an instruction and a time and 
	 * enque's them to the back of the given queue.
	 *
	 */


