Assignment 2 - Dylan Thompson - 1000260


How to compile my program:
	-In the directory that the files are contained in, execute the following command
		$ gcc -oassign2.x assign2.c	
	-This will summon gcc to compile the code and create a program in the executable file called assign2.x
	
	
How to run my code:
	-In the derectory where the executable file is stored, execute the following command to run my program:
		$ ./assign2.x <filename>
	 where <filename> is the name of the text file that you want to send as input.
	-If you wish to automatically run the included file called sampleInput.txt as input,
	 simply run the following command instead:
		$ ./assign2.x
	-When you run the program with out an argument, it will search for that file and use it as input.
	-Note: my program will use this input file four times during its execution, so this file must remain
			available to the program for the duration of execution. 
